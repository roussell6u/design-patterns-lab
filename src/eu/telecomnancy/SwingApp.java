package eu.telecomnancy;

import eu.telecomnancy.sensor.Adapteur;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.Proxy;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
        Proxy sensor = new Proxy();
        new MainWindow(sensor);
    }

}
