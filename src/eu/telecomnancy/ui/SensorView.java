package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.Adapteur;
import eu.telecomnancy.sensor.Decorator;
import eu.telecomnancy.sensor.DecoratorCelcius;
import eu.telecomnancy.sensor.DecoratorFahrenheit;
import eu.telecomnancy.sensor.DecoratorRound;
import eu.telecomnancy.sensor.Proxy;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class SensorView extends JPanel implements Observer{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Proxy sensor;
	private Decorator d;
    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private JButton celcius = new JButton("Celcius");
    private JButton fahrenheit = new JButton("Fahrenheit");
    private JButton round = new JButton("Round");

    public SensorView(Proxy c) {
        this.sensor = c;
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sensor.on();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sensor.off();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    sensor.update();
                    value.setText(String.valueOf(sensor.getValue())+" °C");
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                }
            }
        });
        
        celcius.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				d=new DecoratorCelcius();
				try {
					d.execute(value,sensor);
				}
				catch(SensorNotActivatedException sensorNotActivatedException){
					sensorNotActivatedException.printStackTrace();
				}
			}
		});
        
        fahrenheit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				d=new DecoratorFahrenheit();
				try {
					d.execute(value, sensor);
				} catch (SensorNotActivatedException sensorNotActivatedException) {
					sensorNotActivatedException.printStackTrace();
				}
				
			}
		});
        
        round.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				d=new DecoratorRound();
				try {
					d.execute(value, sensor);
				} catch (SensorNotActivatedException sensorNotActivatedException) {
					sensorNotActivatedException.printStackTrace();
				}
			}
		});

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 6));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        buttonsPanel.add(celcius);
        buttonsPanel.add(fahrenheit);
        buttonsPanel.add(round);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

	@Override
	public void update(Observable o, Object arg) {
		if (arg instanceof Adapteur) {
			try {
				value.setText(String.valueOf(sensor.getValue())+" °C");
			}catch(SensorNotActivatedException sensorNotActivatedException){
				sensorNotActivatedException.printStackTrace();
			}
		}
		
	}
}
