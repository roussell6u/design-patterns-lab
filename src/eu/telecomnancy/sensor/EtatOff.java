package eu.telecomnancy.sensor;

public class EtatOff implements Etat{
	public boolean statut;
	
	public EtatOff(){
		statut=false;
	}
	
	public boolean getStatut(){
		return statut;
	}
}
