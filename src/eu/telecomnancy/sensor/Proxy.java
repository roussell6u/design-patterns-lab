package eu.telecomnancy.sensor;

import java.util.Date;

public class Proxy implements ISensor{
	protected ISensor s;
	
	public Proxy(){
		s=new Adapteur();
	}
	
	public void on(){
		log("on()","void");
		s.on();
	}
	
	public void off(){
		log("off()","void");
		s.off();
	}
	
	public boolean getStatus(){
		boolean t=s.getStatus();
		log("getStatus()","boolean");
		return t;
	}
	
	public void update() throws SensorNotActivatedException{
		log("update()","void");
		s.update();
	}
	
	public double getValue() throws SensorNotActivatedException{
		log("getValue()","double");
		return s.getValue();
	}
	
	public void log(String fct,String ret){
		System.out.println(new Date()+" Appel méthode/fonction : "+fct+" qui retourne un "+ret);
	}
}
