package eu.telecomnancy.sensor;

import javax.swing.JLabel;

public class DecoratorCelcius implements Decorator{

	@Override
	public void execute(JLabel l, Proxy s) throws SensorNotActivatedException{
		l.setText(String.valueOf(s.getValue())+" °C");
	}
}
