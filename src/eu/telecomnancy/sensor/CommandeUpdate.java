package eu.telecomnancy.sensor;

public class CommandeUpdate implements Commande{
	ISensor s;
	
	public CommandeUpdate(ISensor s){
		this.s=s;
	}
	
	public void execute() throws SensorNotActivatedException{
		try{
			s.update();
		}catch(SensorNotActivatedException e){
			System.out.println("Sensor must be activated to get its value.");
		}
	}
}
