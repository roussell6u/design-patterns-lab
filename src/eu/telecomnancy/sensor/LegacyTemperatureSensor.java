package eu.telecomnancy.sensor;

import java.util.Random;

public class LegacyTemperatureSensor{
    private Etat e=new EtatOff();
    private double start = -20;
    private double end = 100;
    private double value;
    private AcquiringThread worker = null;

    /**
     * Enable/disable the sensor.
     *
     * @return the current sensor status.
     */
    public synchronized boolean onOff() {
        changeEtat();
        if (e.getStatut()) {
            this.worker = new AcquiringThread();
            this.worker.start();
        } else {
            this.worker.disable();
            this.worker = null;
        }

        return this.getStatus();
    }
    
    public void changeEtat(){
    	if (e instanceof EtatOn) {
			e=new EtatOff();
		}
    	else{
    		e=new EtatOn();
    	}
    }

    /**
     * Get the status (enabled/disabled) of the sensor.
     *
     * @return the current sensor's status.
     */
    public boolean getStatus() {
        return e.getStatut();
    }

    /**
     * Get the current temperature.
     *
     * @return the latest recorded temperature.
     */
    public double getTemperature() {
        return this.value;
    }

    private class AcquiringThread extends Thread {
        private boolean active = true;

        /**
         * Stop the acquiring thread. There is no way to start it again ;)
         */
        public void disable() {
            this.active = false;
        }

        @Override
        public void run() {
            while (this.active) {
                double random = (new Random()).nextDouble();
                value = start + (random * (end - start));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
