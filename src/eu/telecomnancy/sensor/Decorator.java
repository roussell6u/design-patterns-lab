package eu.telecomnancy.sensor;

import javax.swing.JLabel;

public interface Decorator {
	public void execute(JLabel l, Proxy s) throws SensorNotActivatedException;
}
