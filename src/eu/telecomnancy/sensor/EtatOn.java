package eu.telecomnancy.sensor;

public class EtatOn implements Etat{
	protected boolean statut;
	
	public EtatOn(){
		statut=true;
	}
	
	public boolean getStatut(){
		return statut;
	}
}
