package eu.telecomnancy.sensor;

import javax.swing.JLabel;

public class DecoratorFahrenheit implements Decorator{

	@Override
	public void execute(JLabel l, Proxy s) throws SensorNotActivatedException {
		double d=s.getValue()*1.8+32;
		l.setText(String.valueOf(d)+" °F");
	}
}
