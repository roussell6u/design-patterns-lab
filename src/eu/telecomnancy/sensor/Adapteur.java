package eu.telecomnancy.sensor;

import java.util.Observable;

public class Adapteur extends Observable implements ISensor{
	protected LegacyTemperatureSensor l=new LegacyTemperatureSensor();

	public void on(){
		if (!this.getStatus()) {
			l.onOff();
		}
	}

	public void off(){
		if (this.getStatus()) {
			l.onOff();
		}
	}

	public boolean getStatus(){
		return l.getStatus();
	}

	public void update() throws SensorNotActivatedException{
		try{
			l.onOff();
			l.onOff();
			setChanged();
			notifyObservers();
		}catch(Exception e){
			System.out.println("Sensor must be activated to get its value.");
		}
	}

	public double getValue() throws SensorNotActivatedException{
		try{
			return l.getTemperature();
		}catch(Exception e){
		System.out.println("Sensor must be activated to get its value.");
		return 0;
		}
	}
}