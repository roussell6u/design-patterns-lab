package eu.telecomnancy.sensor;

import javax.swing.JLabel;

public class DecoratorRound implements Decorator{

	@Override
	public void execute(JLabel l, Proxy s) throws SensorNotActivatedException {
		long i=Math.round(s.getValue());
		l.setText(i+" °C");
	}

}
